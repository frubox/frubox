# Humanidades

Material de estudio para la Licenciatura en Filosofía de la UNSAM (escuela de humanidades).

???+ question
    No recibimos aportes para esta sección hasta ahora.

    Escribinos a <unsam@frubox.org> con tus aportes para inaugurarla. 😊

    :tomato: :lemon: :watermelon: :tangerine: :grapes: :avocado: :hot_pepper: :cherries: :pineapple: :kiwi: :strawberry: :apple: :blueberries: :coconut:

## 🗃️ Recursos

[Library Genesis](http://www.google.com/url?q=http%3A%2F%2Fgen.lib.rus.ec%2F&sa=D&sntz=1&usg=AOvVaw0QjABkyBpgeu2MsihpC4Bm)

[Filosofía del Idealismo](http://www.google.com/url?q=http%3A%2F%2Fwww.unsam.edu.ar%2Fidealismo%2F&sa=D&sntz=1&usg=AOvVaw09_zXaja8YzZieRA-tBvLz)

[Reglamento Oficial de Adscripciones UNSAM](http://www.google.com/url?q=http%3A%2F%2Fwww.unsam.edu.ar%2Fescuelas%2Fhumanidades%2Farchivos%2FAdscripciones%2520Reglamento.pdf&sa=D&sntz=1&usg=AOvVaw26wKtGrURDuGumkjdoGKEy)

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**