# Cálculo I

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

🛠️ [Calculadora de derivadas](https://www.calculadora-de-derivadas.com/)

🛠️ [Calculadora de integrales](https://www.calculadora-de-integrales.com/)

## 💭 Experiencias

- Conviene tomarse el trabajo de hacer los ejercicios pedidos por el sistema de vidas a conciencia. No sirve de nada copiarlos, porque el sistema ayuda a llevar al día la materia, y permite ver cómo está posicionado uno frente a los temas (además de saber qué es lo que se espera que se justifique en cada ejercicio). Si uno va haciendo de a poco las guías y preguntando todo lo que no termina de entender, se llega tranquilo a los parciales.

✏️ Escribinos con tu aporte a <unsam@frubox.org>.


## 📱 Grupos de whatsapp

💬[Cálculo I](https://chat.whatsapp.com/F3EpAtuYn5p9ono9jPqbon)

## 📂 Carpeta Frubox

***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)***

<iframe src="https://drive.google.com/embeddedfolderview?id=1bfMYINO9ipaXEsxUGkp4fzZMLMaooBqU" style="width:100%; height:600px; border:0;"></iframe>
