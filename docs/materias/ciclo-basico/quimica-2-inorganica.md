# Química Inorgánica

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

 Linda materia.
 
 andy4ever


✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.

## 📚 Material Extra Externo

"Guías teóricas para complementar o sacar información para química inorgánica" [sites.google.com/a/agro.uba.ar/quimica-aplicada/](http://www.google.com/url?q=http%3A%2F%2Fsites.google.com%2Fa%2Fagro.uba.ar%2Fquimica-aplicada%2F&sa=D&sntz=1&usg=AOvVaw0URhedGW5OVWUeIa33Dtzy)



## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)***
 

<iframe src="https://drive.google.com/embeddedfolderview?id=1eL0n9L70pZR93RoHGBks5BmmUF-Bs3AU" style="width:100%; height:600px; border:0;"></iframe>
