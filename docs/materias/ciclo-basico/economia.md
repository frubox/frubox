# Economía

???+ warning  "Atención"
	**Hay varias páginas para las economías...**

	-   Ambiental: <https://wiki.frubox.org/materias/ing-ambiental/economia>
	-   Ciclo basico: <https://wiki.frubox.org/materias/ciclo-basico/economia>
	-   Microeconomia: <https://wiki.frubox.org/materias/ciclo-basico/microeconomia>

	**Quizás lo que buscás está en una y no en la otras.**


📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

Materia complicada para los que no estamos en tema generales de economía. Por ahora es lo que hay en el plan de estudio, pero vale aclarar que no es la encomia que los ingenieros ambientales necesitamos. Nosotros deberíamos tener otro enfoque, una "economía ambiental" como en otras universidades.

Se recomienda entrar al siguiente canal de YouTube:

[http://www.youtube.com/playlist?list=PLa1CmC-O00yYQ0WpzZ\\\_y9H2w\\\_MtxIcxR0](http://www.youtube.com/playlist?list=PLa1CmC-O00yYQ0WpzZ\_y9H2w\_MtxIcxR0)

Los videos son bastante cortos y muy buenos.

Hay varias páginas para las economías:

-   [http://www.frubox.org/home/ciclo-basico/economia](http://www.google.com/url?q=http%3A%2F%2Fwww.frubox.org%2Fhome%2Fciclo-basico%2Feconomia&sa=D&sntz=1&usg=AOvVaw2gOHnRNux3KnPgdxcQ7AYL)
-   [http://www.frubox.org/home/ciclo-basico/microeconomia](http://www.google.com/url?q=http%3A%2F%2Fwww.frubox.org%2Fhome%2Fciclo-basico%2Fmicroeconomia&sa=D&sntz=1&usg=AOvVaw3qZCcmLAhzJ1Kni4dMrNGC)
-   [http://www.frubox.org/home/ambiental/economia](http://www.google.com/url?q=http%3A%2F%2Fwww.frubox.org%2Fhome%2Fambiental%2Feconomia&sa=D&sntz=1&usg=AOvVaw1owte0ojSJdtvLY4GmcgsJ)


## 📱 Grupos de whatsapp

💬[Economía](https://chat.whatsapp.com/FJAAMqfllM3ERJ0Jk5ffZJ)


## 📂 Carpeta Frubox


<iframe src="https://drive.google.com/embeddedfolderview?id=1SM9JNT5QEblZImAfRdt3evdt37lMoK9Y" style="width:100%; height:600px; border:0;"></iframe>
