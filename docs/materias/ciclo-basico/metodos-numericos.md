# Métodos Numéricos

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](/frubox/biblioteca)

## 💭 Experiencias

 Mis claves para aprobar la materia: - Para el pre final aprovechar mucho las clases prácticas. Si la cursan con Guido prácticamente dejan los códigos listos que podés usar en el examen y te ahorran mucho tiempo. Anotar todo lo que dicen los profes. Ya sea conceptos (Guido había cosas que decía que tomaba literal lo mismo) o el paso a paso de los códigos (ayuda mucho a después entender que estás haciendo). A mi personalmente me sirvió más hacer pre finales de otros cuatris que las guías. - Para el final entender bien bien la teoría. Toma ejercicios pero con base fuerte de teoría. Ver las clases, tomar apuntes y estudiar de ellos es clave para aprobar.

Sitio Oficial:

<http://numericounsam.blogspot.com.ar/>

Métodos Numéricos para tecnicaturas (?) [Metodos Numericos (TPI/TRI)](..//tecnicaturas/metodos-numericos-tpi-tri.html)

## 📱 Grupos de whatsapp

💬[Métodos Numéricos](https://chat.whatsapp.com/G6DNMTPhxtFJusRmZHktC1)


## 📂 Carpeta Frubox

***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)***
 

<iframe src="https://drive.google.com/embeddedfolderview?id=19oY3gzU4Wrk86GckK4UyX2lPfR9eW0Zj" style="width:100%; height:600px; border:0;"></iframe>
