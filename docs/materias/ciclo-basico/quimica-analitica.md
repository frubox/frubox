# Química Analítica

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

- "Recomendamos mucho hacerla tan pronto después de orgánica como puedas".

- **Ojo, las profesoras saben que tenemos modelos de parcial pero no les agrada demasiado, y menos aún que tengamos resueltos. Es por eso que se pide discresión al respecto.**

- A partir del 2020 la materia se empezó a dar para LBT también, el primer cuatrimestre está más orientado a LBT y el segundo a Ing Ambiental, pero en realidad son lo mismo, se da el mismo material ambos cuatrimestres.


✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

 

<iframe src="https://drive.google.com/embeddedfolderview?id=1bRq1BtydiHbDlnDXAZzYr7mRlHVtg-3A" style="width:100%; height:600px; border:0;"></iframe>
