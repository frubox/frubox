# Física I

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

Aca dejo un link con unas clases del MIT muy utiles para Fisica 1: [http://videolectures.net/mit801f99_physics_classical_mechanics/](http://www.google.com/url?q=http%3A%2F%2Fvideolectures.net%2Fmit801f99_physics_classical_mechanics%2F&sa=D&sntz=1&usg=AOvVaw0xoC6dOdFrtZs3goNbvrfr)\
Y otro canal muy util: <https://www.youtube.com/user/canalusb/playlists>

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬[Física I](https://chat.whatsapp.com/HB9W2mdyHeE5tiVBWpXAYx)


## 📂 Carpeta Frubox

 ***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)***

<iframe src="https://drive.google.com/embeddedfolderview?id=1TLhwCIoL5qbFzbf2a-mnZ7peiFmcGQKA" style="width:100%; height:600px; border:0;"></iframe>
