# Química General

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

Generalidades de la Química!

A veces el final es semi-oral, y un compañero nos deja la siguiente descripción de esta modalidad, que también se ha visto en Química Inorgánica con Andrés Zelcer.

_**Examen Final Oral-Escrito**_

Hoy (2015/12/10) me tomaron el final y fue de la siguiente manera:

Nos dieron a cada uno dos temas de los 12 que vimos en toda la cursada, y dos horas para redactar todo lo que sepamos. Después de eso, el profe venía a tu banco, y mientras leía lo que habíamos escrito, nos iba cuestionando las cosas que escribimos para ver hasta que punto estábamos seguro de lo que pusimos.

También me tomó cosas que no llegué a poner, que no se entendían bien, o que puse y estaban mal. Me planteó un par de problemas, y me preguntó por dónde los encararía. Fue todo teórico, pero muy, muy minucioso.

Cabe destacar que esto fue en el primer llamado.

​Saludos​ y aguante Frubox!

 (¡Andy pasión!)



## 📱 Grupos de whatsapp

💬[Química General](https://chat.whatsapp.com/HQLkik0Baf1Dy0BZr3Kc7C)


## 📂 Carpeta Frubox

 ***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)***

<iframe src="https://drive.google.com/embeddedfolderview?id=1xziYmKanKnJcAzgH_3M3ICY5ls6xYEdJ" style="width:100%; height:600px; border:0;"></iframe>
