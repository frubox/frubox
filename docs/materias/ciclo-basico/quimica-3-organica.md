# Química Orgánica

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

- Duffó es un capo.

- Si tenés química analítica en el programa, mejor hacerla tan pronto como se pueda después de cursar química orgánica.


✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬[Ética y Ejercicio Prof](https://chat.whatsapp.com/BXTPnPJ3UWt1Uy6AgyUSRJ)

💬[Química Orgánica Alumnos](https://chat.whatsapp.com/HCDHCJImItM7zEH2uObDaJ)

## 📂 Carpeta Frubox

***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)***
 

<iframe src="https://drive.google.com/embeddedfolderview?id=1CNMHpqpgpU1vm8HhKl-SVshvNCBpyoAa" style="width:100%; height:600px; border:0;"></iframe>
