# Física IV

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

**Simulaciones**

Hay muchas páginas que muestran gráficamente las soluciones y conceptos detrás de los sistemas cuánticos simples que se ven en la materia. Sugerencia: [http://www.st-andrews.ac.uk/physics/quvis/](http://www.google.com/url?q=http%3A%2F%2Fwww.st-andrews.ac.uk%2Fphysics%2Fquvis%2F&sa=D&sntz=1&usg=AOvVaw0vKGu528iFEITZYRpmwjfs)\
Ese sitio está muy bueno porque tiene muchos ejemplos, y permite explorar cada uno para aprender los conceptos detrás de cada situación. Además tiene mini cuestionarios para porder autoevaluar la comprensión del problema.\
Videos Explicativos

**AK Lectures**

<https://www.youtube.com/user/mathdude2012>

[https://www.youtube.com/user/mathdude2012/playlists?sort=dd&view=50&shelf\\\_id=14](https://www.youtube.com/user/mathdude2012/playlists?sort=dd&view=50&shelf\_id=14)

<https://www.youtube.com/playlist?list=PL9jo2wQj1WCNhP1MFMX7PLbFTu0BgWqz4>

**Probability of Electron in Rigid Box Example**

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupos de whatsapp

💬[FISICA IV](https://chat.whatsapp.com/K3JavgrV2cEJmwTN9n89nj)


## 📂 Carpeta Frubox

***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)*** 

<iframe src="https://drive.google.com/embeddedfolderview?id=1B-20zufbuKeM32eDTcYSkdNctxE_5Ze3" style="width:100%; height:600px; border:0;"></iframe>
