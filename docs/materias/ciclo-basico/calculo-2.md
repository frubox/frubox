# Cálculo II / Análisis II

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

🛠️ [Calculadora de derivadas](https://www.calculadora-de-derivadas.com/)

🛠️ [Calculadora de integrales](https://www.calculadora-de-integrales.com/)

## 💭 Experiencias

Cuando cursé esta materia (con Rosa y León, 2014) las postas fueron:

1.  Aprovechar las consultas por mascampus, León es un crack por este medio.
2.  Entender la teoría de fondo te dice que hacer en cada situación (y te sentís iluminado/a) y en ese entonces Rosa te quería si mostrabas interés.

Como no siempre entendía ni preguntaba todo, recurrí a otros recursos recontra claves:

-   Cursos en línea del MIT o "*[MIT Open Courseware](http://www.google.com/url?q=http%3A%2F%2Focw.mit.edu%2Fcourses%2Ffind-by-topic%2F&sa=D&sntz=1&usg=AOvVaw0wp0ke-S_DWVxgMQiLOSTB)*".
-   Las notas de Paul: "*[Paul's Online Math Notes](http://www.google.com/url?q=http%3A%2F%2Ftutorial.math.lamar.edu%2F&sa=D&sntz=1&usg=AOvVaw2fHShTXn24fxz_TowsMjG8)*".
-   Resolver exámenes anteriores, muchos.
-   Estudiar con <compañer@s>!
-   Utilizar [éste canal de YouTube](https://www.youtube.com/channel/UCXGdlHk6tW32DweB6dJf-BA) donde hay ejercicios resueltos!
-   Material de [análisis 2](http://www.google.com/url?q=http%3A%2F%2Fanalisis2.com%2F&sa=D&sntz=1&usg=AOvVaw3JfKFBm5L9scRNL5vfyi_p) de FIUBA.

Lo del MIT es lo que suena, graban las clases y te dan todo el material de cursada. ¡Las [clases de cálculo en video con el francecito](http://www.google.com/url?q=http%3A%2F%2Focw.mit.edu%2Fcourses%2Fmathematics%2F18-02-multivariable-calculus-fall-2007%2F&sa=D&sntz=1&usg=AOvVaw01RtLs_Lnheq0F13m3Qwwr) (en inglés) son muy buenas! Las que usé, para la segunda parte especialmente, están subidas acá.

Ya que estoy, la **antiposta** es perderse clases y no practicar.

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupos de whatsapp

💬[Cálculo II](https://chat.whatsapp.com/H72CUbeVOfm8RTtQugpSVB)

## 📂 Carpeta Frubox

***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)***
 

<iframe src="https://drive.google.com/embeddedfolderview?id=1RR1dKMEvzfEtspGlaNha309e-3oeUMYd" style="width:100%; height:600px; border:0;"></iframe>
