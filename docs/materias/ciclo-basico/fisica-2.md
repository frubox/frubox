# Física II

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 📱 Grupos de whatsapp

 💬 [Física II Noche](https://chat.whatsapp.com/Dav58wCHTYnD1yPxN3JkIn)
 
 💬 [Fisica II Ingenierías](https://chat.whatsapp.com/G4eJyJLWApqEDA61BWWNGJ)



## 📂 Carpeta Frubox

 ***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)***

 <iframe src="https://drive.google.com/embeddedfolderview?id=1Rdt_uGMGn_RIMp8XcQuZn__vUd1CRrog" style="width:100%; height:600px; border:0;"></iframe>
