# Cálculo Avanzado

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

Muchas carreras pueden hacer Calculo 3 en vez de Avanzado y se considera aprobada, pero eso es un terrible error porque **cálculo 3 es mas difícil**.

La materia la da Marcela Morvidone, arranca tranqui, y de un día para el otro mete 5º. Así que no se duerman. Final obligatorio. Un parcial mas dos TP de matlab para aprobar la cursada. 

[http://tutorial.math.lamar.edu/Classes/DE/DE.aspx](http://www.google.com/url?q=http%3A%2F%2Ftutorial.math.lamar.edu%2FClasses%2FDE%2FDE.aspx&sa=D&sntz=1&usg=AOvVaw1Ork_dzfPTp5oD2unW9-xp) 

Esta pagina es absolutamente invaluable. Contiene todos los contenidos de ecuaciones diferenciales muy claros y ordenados, me salvo la materia. Lamentablemente solo en ingles. 

[https://bluffton.edu/homepages/facstaff/nesterd/java/slopefields.html](https://www.google.com/url?q=https%3A%2F%2Fbluffton.edu%2Fhomepages%2Ffacstaff%2Fnesterd%2Fjava%2Fslopefields.html&sa=D&sntz=1&usg=AOvVaw1a8z-Rp_8jrdzBSwy1qV2v)

Esta pagina es usada durante toda la cursada para graficar campos de pendientes.

(Juan)

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp
**-**


📂 Carpeta Frubox

***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)*** 

<iframe src="https://drive.google.com/embeddedfolderview?id=1zqNYjJl4G3sqCSg0kSXQkVxK4iBeyoWh" style="width:100%; height:600px; border:0;"></iframe>
