# Álgebra y Geometría Analítica I

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


Para febrero de 2016 se armó un curso intensivo de verano! Gracias a la movida del CECyT 2016.


## 📱 Grupo de whatsapp

[Álgebra y Geometría Analítica I](https://chat.whatsapp.com/Edd51SsLevcFPFL63d9Op2)



## 📂 Carpeta Frubox

***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)***

<iframe src="https://drive.google.com/embeddedfolderview?id=1HP8h9kgSmF2gaFuN9FRQHpL6X-MGOZaH" style="width:100%; height:600px; border:0;"></iframe>
