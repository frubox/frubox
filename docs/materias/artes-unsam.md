# Artes

???+ question
    He aquí la sección para la escuela de Artes de la UNSAM 😊

    Hola! Este es tu espacio, nuestro espacio, futures licenciades. Acá vas a encontrar todo el material que necesites para que juntes le hagamos frente a esta hermosa carrera; y ayudemos a aplanar el camino para camadas futuras. ¿Te sumás?

    Para enviar material, porfa seguí estas consignas:

    - Subí tus apuntes identificando la materia y el año.
    - Subí tus fotos en 96dpi, 2000px lado largo, 80% calidad; cada trabajo en su respectiva carpeta. (Sip, como en el CPU, que sino el almacenamiento explota(?)

    Para consultas sobre cómo enviar material de Lic. en Fotografía u otras cosillas, comunicate con Tomate🍅 a <unsam@frubox.org>.

    Opiniones, reseñas, comparaciones con otras carreras/universidades.

    Planes de estudio y recomendaciones para organizar los cuatrimestres.

    Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. 
    
    ✏️ Escribinos con tu aporte a <unsam@frubox.com>

## 💭 Experiencias

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

<iframe src="https://drive.google.com/embeddedfolderview?id=1nyqzH6eznoKLRDNNzQ3ftJySokPgUSb9" style="width:100%; height:600px; border:0;"></iframe>
