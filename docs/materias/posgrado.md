# Posgrado

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

Estaría bueno tener un "manual del becario", si es que elegís ese camino.

Evitaría bocha de frustración a mucha gente que quiere entrar al sistema CyT.

### Elegir un lugar

Aunque pensé este post en relación a la búsqueda de un lugar para hacer la tesina y el doctorado, creo que es un poco más genérico, y puede servir para otras búsquedas.

Algunos puntos importantes:

- Calidad del trato interpersonal.
- Capacidad de trabajo de las personas.
- Recursos materiales del lugar.
- El tema de trabajo.
- Conexiones con otros laboratorios, locales y en el exterior especialmente.
- Si faltara alguna de estas en tus opciones, seguramente haya un mejor lugar para vos 🙂

Para hacerse una idea, conviene visitar al menos 3 lugares diferentes. Si te da fiaca hacelo igual.

### Preguntas para hacer

Valen para hacer la tesina y el doctorado:

- ¿Cómo se llevan en el labo? ¿Cada cuanto se ven por fuera? ¿Qué actividades de socialización oranizan?
- ¿Cómo es la relación del director con sus becaries?
- ¿Es bueno el trato interpersonal?
- ¿Está presente o demasiado ocupado? ¿Cada cuanto tienen reuniones individuales?
- Preguntar qué tanto labura la gente ahí, y que tan “bien”. Se puede pedir referencias externas.
- ¿Está bien financiado el laboratorio? ¿Hay para gastar plata en ideas propias?
- ¿Funciona bien el instituto que aloja al laboratorio? ¿Que servicios institucionales ofrece? (autoclave, preparacion de medios, lavado de material, personal dedicado a compras, etc.).
- ¿Te interesa el tema? ¿Quién viene trabajando en el tema que te ofrecen? ¿Cómo les fue con eso? ¿Publicaron?
- ¿Qué hizo la gente después de estar ahí? ¿Qué opinan del lugar? ¿Hubo gente que dejó?


## 📱 Grupos de whatsapp

💬 **-**