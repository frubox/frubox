# Materias UNSAM 📝

> Encontrá material de las materias más abajo!

Botones útiles:

[📚 Biblioteca](/biblioteca){ .md-button }
[✉️ Enviar aporte](mailto:unsam@frubox.org?subject=Aporte){ .md-button .md-button--primary }

## Material de las materias por carrera

Para encontrar una carrera, usá la **barra de navegación** a la izquierda, o bien **el buscador**.

Vas a encontrar ahí Ahora las carreras y materias para las que recibimos aportes!

Si falta una materia, solo hace falta que nos manden un aporte para armarle su página.

## Buscador

Usá la barra de búsqueda para encontrar tus materias.

**Importante**: El buscador es sensible a los **tildes**, si buscás "cálculo" quizás no encuentres "cálculo".

## Aportes

¿Tenés un aporte para subir? mandanos un mail a <unsam@frubox.org> y nos encargamos :)

Si te copás, lo que tengas de otros años mandalo también. Vos lo enviás, nosotros le damos formato, te agradecemos y lo subimos.

Todo esto es gracias al esfuerzo de la comunidad estudiantil de la UNSAM, desde 2013 hasta hoy. Con tu aporte podés ayudar a mucha gente.
