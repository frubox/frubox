# Biología CPU (TDI/TEM)

## 💡 Como aprobar Biologia CPU y no morir en el intento

Hola, a vos que sos nuevo en la universidad o estas recursando...bienvenido/a!

- Andá a las clases, hacete los cuestionarios, aprobá los parcialitos, pregunta TODAS tus dudas (por más mínimas que sean) y hacete resumenes.

**Ponete a resolver parciales a full**

 Si ya estuviste jugando un poco con la guía de ejercicios, sumale resolver parciales. La estructura de ejercicios es casi siempre la misma, los mismos ejercicios "clave" e "integradores" que evalúan varios contenidos al mismo tiempo. Esto aplica también cuando estés adentro de la carrera: parciales, parciales y más parciales. Igual no te confíes con solo resolver ejercicios de parciales viejos, la teoría hay que leerla siempre.

- Tené en cuenta que los recuperatorios caen todos juntos junto con el cierre de las materias y los segundos parciales, así que cuantos menos recuperatorios tengas que dar, mejor la vas a pasar. 😄

- No subestimes a la materia.

- Si en una de esas llegás a recursar alguna/s materia/s no pasa nada: estudiá más para la próxima y sacate mil. Lo importante es que no abandones!

## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**

<iframe src="https://drive.google.com/embeddedfolderview?id=1Zh9ovstI78cSGiwz9DEOA94amcbVLMV3" style="width:100%; height:600px; border:0;"></iframe>


