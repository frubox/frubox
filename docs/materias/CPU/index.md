# CPU - Curso de Ingreso

[✉️ Aportar](mailto:unsam@frubox.org?subject=AporteCPU){ .md-button .md-button--primary }

## ¡Bienvenido a la UNSAM!

¡Hola! El CPU es el primer paso en la facu, así que tratamos de recopilar la mayor cantidad de información posible para que te sea mas fácil el trayecto.

No desesperes, el cambio de ritmo de la secundaria a la facu se siente pero vas a agarrarle la mano enseguida 😄

- Información oficial del CPU: <http://www.unsam.edu.ar/escuelas/ciencia/_CPU.asp>

- Sitio de IEU: <http://ieucytunsam.blogspot.com.ar/>

![20220213-154608.png](./20220213-154608.png)

> Don't panic!

## 💡 Información importante CPU

Si necesitás una mano con el CPU, usá a tus profesores y compañeros, pedí clases particulares y/o ayuda (consultá ejercicios, lo que sea) en [Unsamers](http://www.facebook.com/groups/unsamers) , [FruboxUnsam](http://www.facebook.com/groups/fruboxunsam) y [EECyT](http://www.facebook.com/groups/eecyt), usá el material de este sitio. Además, la universidad organiza clases de consulta para estudiantes del CPU, asegurate de aprovecharlas.

Si te animás, envianos un correo con el material que tengas a <unsam@frubox.org> para que esté disponible para otros ingresantes. 

Estudiá, aprendé y aprobá. Esto recién empieza.

Hay una página con info. sobre las **[correlatividades del cpu](./correlatividades-cpu)**. Es importante que las tengan presentes para saber qué pueden cursar y qué les conviene cursar.
