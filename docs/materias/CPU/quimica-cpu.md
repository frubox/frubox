# Química CPU

## 💡 Cómo aprobar Química CPU y no morir en el intento.


Hola, a vos que sos nuevo/a en la univ o estas recursando...bienvenido/a!
    

???+ note

    Esta guía de recomendacion es parecida (pero NO igual: atenti!) a las que hay para las otras materias.
    
    ¿Qué es lo nuevo de esta recomendación? 
    
    - Antes tenia armados unas "recomendaciones" generales sobre cada materia del CPU. Decidimos simplificarlo y re-escribirlas especificamente para cada materia, 10 "preguntas/respuestas", con el fin de hacerlo mas corto y directo al grano, para así poder ayudarte mejor.
    
 
1) **En el secundario no aprendí nada de Matematica... ¿Estoy al horno?**

 - No. Vas a tener que meterle garra y llevar la materia al día. Los libros de ASIMOV que están en la carpeta llamada "Teoría (la possssta)" te pueden ayudar. Además fijate en frubox en la carpeta de ejercicios resueltos, los resueltos de la uba "paso a paso" que vienen con explicación, son LA POSTA. Si, son de otra universidad... pero el formato y dificultad de los ejercicios son prácticamente iguales que en CPU. 
 
 - Es importante saber resolver los ejercicios así que prestale atención a eso, hay muchos también de la materia subidos. 

   - Los libros de Logikamente también son un buen complemento, todo lo podes ver/descargar acá desde Frubox.
   
2) **Matematica del CPU... ¿Es una materia dificil?**

Depende. Si tu caso es super grave y no viste NADA de matemática en el secundario, entonces se puede volver un poquito denso, pero nada imposible. Los temas mas difíciles son: Trigonometría (1er parcial) y ecuaciones exponenciales y logarítmicas (segundo parcial). Esta, como todas las materias exactas sigue la misma ley, hacé ejercicios!!!!

3) **Me cuestan las ciencias exactas... ¿Estoy al horno?**

No, no estás al horno. 

La materia está preparada para recibir estudiantes con distintos niveles de habilidad y darles las herramientas necesarias para que se defiendan durante la carrera. Si te cuesta, puedes apoyarte en los profesores, que están para ayudarte. También hay clases de apoyo y bancos de apuntes. Nunca tengas miedo de preguntar o pedir ayuda con lo que más te cueste. Recordá que están todos en el mismo barco y no todos tienen las mismas facilidades. A veces cuesta más, toma más trabajo pero de la facultad se recibe el/la persistente. Estudia, practica y si sale mal… se intenta otra vez! No está muerto quien pelea.

4) **¿Puedo aprobar la materia sin saber alguna de las series/unidades?**

No. Imposible. Además a partir del 2018  se implementó una nueva modalidad en la que tenés que aprobar 2 parciales para aprobar la materia, cuando antes estaba todo junto en un solo parcial. Así que vas a tener que saber todas las unidades, pero lo bueno es que estudias menos temas por parcial.

5) **¿Cuál es el secreto para aprobar?**

- La constancia, aprueba el/la constante, se recibe el/la constante. 
- Hacer ejercicios es fundamental.
- Prepará los exámenes con tiempo, no los subestimes. Estudiá de parciales viejos.
- No procrastines. A veces la materia parece que va lenta, pero si te dormís te come. Llevala al día.



6) **Me toco un mal profesor de Física CPU y no le entiendo nada. ¿Qué hago?**

Relax, primero, fijate si te podés cambiar de grupo (esto no suele pasar porque los cupos suelen estar llenos pero con probar no se pierde nada). Segundo, aprovechá todo el material subido hay teóricas y ejercicios que te pueden servir de herramientas, preguntá mil veces si hace falta, preguntá a tus compañeros/as, preguntá  en [FruboxUnsam](https://www.google.com/url?q=https%3A%2F%2Fwww.facebook.com%2Fgroups%2Ffruboxunsam&sa=D&sntz=1&usg=AOvVaw1aXTZAFm5cZ5Wa9PMqcedR), Unsamers,[EECyT](https://www.google.com/url?q=https%3A%2F%2Fwww.facebook.com%2Fgroups%2Feecyt&sa=D&sntz=1&usg=AOvVaw2DJWNra0c1ahoZWXkzKabb).

Si te cuesta seguir al profesor en algun tema particular, recurrí a youtube como apoyo. Es una buena herramienta para repasar y profundizar en los temas que cuestan más.


7) **En resumen: Qué me recomendás para aprobar esta materia sí o sí?**

**Ponete a resolver parciales a full**

- Si ya estuviste jugando un poco con la guía de ejercicios, sumale resolver parciales. La estructura de ejercicios es casi siempre la misma, los mismos ejercicios "clave" e "integradores" que evalúan varios contenidos al mismo tiempo. Esto aplica también cuando estés adentro de la carrera: parciales, parciales y más parciales. Igual no te confíes con solo resolver ejercicios de parciales viejos, la teoría hay que leerla siempre.

- Usá los libros de teoría de ASIMOV son para la UBA, pero no te preocupes, el contenido es exactamente el mismo. Incluso varios ejercicios se repiten. De hecho, muchíiisimos docentes de la UNSAM son de la UBA. 

- Tené en cuenta que los recuperatorios caen todos juntos junto con el cierre de las materias y los segundos parciales, así que cuantos menos recuperatorios tengas que dar, mejor la vas a pasar. 😄
    
    
    
8) **Si ya me va mal en el CPU, una vez que este adentro de la carrera...¿Estoy perdido/a?**

No. No estas perdido/a. El CPU es el primer paso entre la secundaria y la facultad, el cambio de ritmo de estudio, la base que puedas traer o no de la secundaria y un montón de cosas están metidas en el medio y pueden joder. Si va mal en el cpu se recursa, se aprende y se pasa a lo siguiente, igual que cuando estés más avanzado/a en la carrera.

Es una etapa que te permite acercarte lentamente a la vida universitaria. A veces cuesta acostumbrarse, los tiempos no son los mismos para todos. ¡A no desesperar!




9) **¿Tenés un particular que me puedas recomendar?**


Preguntá en cualquiera de las páginas de la facu, Unsamers, [FruboxUnsam](https://www.google.com/url?q=https%3A%2F%2Fwww.facebook.com%2Fgroups%2Ffruboxunsam&sa=D&sntz=1&usg=AOvVaw1aXTZAFm5cZ5Wa9PMqcedR) o [Estudiantes CyT UNSAM](https://www.google.com/url?q=https%3A%2F%2Fwww.facebook.com%2Fgroups%2Feecyt&sa=D&sntz=1&usg=AOvVaw2DJWNra0c1ahoZWXkzKabb), y vas a encontrar.


10) **¿Y si no tengo plata para ir a un particular?**


No es obligatorio gastar plata para aprobar. Forma grupos de estudio (organizados en la facu o en los grupos de facebook). Son muy útiles y siempre va a haber un tema que tus compañeros/as entiendan mejor que vos y vice versa. Recordá: Dos cabezas (o más) piensan mejor que una. También en el grupo de facebook podes postear tus dudas sobre ejercicios (si les sacas una foto y la subís genial, así se entiende mejor).

Puede ser que en otras universidades no seas más que un número. Acá no: ¡Te queremos ayudar!



**Comentario final**

- Si en una de esas llegas a recursar alguna/s materia/s del CPU no pasa nada: estudia mas para la próxima y sacate mil. Lo importante es que no abandones!!!

 Estamos para darte una mano. No aflojes. Te va a ir bien.

## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**


## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**

<iframe src="https://drive.google.com/embeddedfolderview?id=1WLfKEQOUZ_dJU43PNzm4IAX8S9tviQ1t" style="width:100%; height:600px; border:0;"></iframe>

