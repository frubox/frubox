# Correlatividades CPU - Primer Año

En esta página pueden encontrar las correlatividades de las materias del CPU con las primeras materias de las ingenierías, licenciaturas y tecnicaturas.

Si hace falta algún cambio, escriban a <unsam@frubox.org>

## Correlatividades de Ing. y Lic.

![20220213-154035.png](./correlatividades-cpu/20220213-154035.png)

## Correlatividades de Tecnicaturas

![20220213-154048.png](./correlatividades-cpu/20220213-154048.png)
