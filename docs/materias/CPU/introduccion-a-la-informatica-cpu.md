# Introducción a la Informática CPU (TPI/TRI)


## 💡 Cómo aprobar Introd. Informática CPU y no morir en el intento.

Hola, a vos que sos nuevo/a en la universidad o estas recursando...bienvenido/a!

???+ note
    Esta guía de recomendación es parecida (pero NO igual: atenti!) a las que hay para las otras materias


- No necesitas más teoría que la que ya aparece en los apuntes, así que no busques en ningún otro lado (no te conviene...con los apuntes alcanza y sobra, si buscas en otro lado solo vas a lograr confundirte mas)

- Llevá la materia al día, hacete las guías de ejercicios, preguntá TODAS las dudas (por mínima que sean) y te va a ir bien.

**Ponete a resolver parciales a full**

 - Si ya estuviste jugando un poco con la guía de ejercicios, sumale resolver parciales. La estructura de ejercicios es casi siempre la misma, los mismos ejercicios "clave" e "integradores" que evalúan varios contenidos al mismo tiempo. Esto aplica también cuando estés adentro de la carrera: parciales, parciales y más parciales. Igual no te confíes con solo resolver ejercicios de parciales viejos, la teoría hay que leerla siempre.

- Tené en cuenta que los recuperatorios caen todos juntos junto con el cierre de las materias y los segundos parciales, así que cuantos menos recuperatorios tengas que dar, mejor la vas a pasar. 😄

- No subestimes a la materia, que es lo que muchos hacen y después les va mal.

- Si en una de esas llegas a recursar alguna/s materia/s no pasa nada: estudia mas para la próxima y sacate mil. Lo importante es que no abandones!


## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**


<iframe src="https://drive.google.com/embeddedfolderview?id=15bseX9HkUSJuuhdnU4agwXDt4nFqJts9" style="width:100%; height:600px; border:0;"></iframe>

