# PFI

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

Para el PFI hay una presentación con los conceptos generales que deben cubrirse en el Proyecto al ser entregado. Esa presentación se da cuando uno se anota a la materia PFI. Se aprueba la cursada presentando una propuesta de Proyecto, que es una introducción a lo que se va a desarrollar. Cuando se aprueba por la cátedra de la materia (<catedrapfi.unsam@gmail.com>), se aprueba la cursada. El final se aprueba el mismo día que se defiende con presentación al Proyecto, previa entrega impresa del mismo, y te tiren compost en la cabeza. ¡Felicidades!

Como vas a ver en la lista de temas, hasta el momento no hubo mucha diversidad. Los temas posibles son:

-   Planta de tratamiento de efluentes industriales
-   Planta de tratamiento de efluentes cloacales y diseño de red cloacal
-   Planta potabilizadora de agua y diseño de red de agua potable
-   Gestión de Residuos Sólidos Urbanos
-   Remediación de suelos
-   Remediación de gases

Si te interesa otra temática, mi sugerencia personal (y vos guiensó?) es que preguntes igual y en base a la respuesta, si es positiva, averigües bien y decidas. Elige tu propia aventura!

La lista de libros que ayudan está en construcción. A veces es difícil conseguir material, así que se aceptan sugerencias. Varios de los sugeridos en las cátedras donde nos enseñan a diseñar plantas de tratamiento están en la Biblioteca UNSAM! También resulta no menor saber que es muy educativo asistir a defensas de PFI para tener un panorama del tema, y que hay copias físicas de ellos disponibles en Biblioteca para consultar. 

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox


<iframe src="https://drive.google.com/embeddedfolderview?id=1y_b0c-Cde6MpwXY7iMuBTOr7XdLUEzlw" style="width:100%; height:600px; border:0;"></iframe>
