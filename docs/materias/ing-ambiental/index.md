# Ambiental

???+ question "He aquí el espacio para las carreras ambientales de la UNSAM:"
    
    -   Licenciatura en Análisis Ambiental
    -   Ingeniería Ambiental

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

**Reseña de estudiante avanzada**

Hola mí nombre es María Eugenia y soy estudiante de último año de ingeniería ambiental. La carrera está muy buena, su duración es de 5 años y medio más medio año de CPU. Los primeros tres años son de ciclo básico y se cursan junto con las otras ingenierías. Sin embargo ya desde el segundo año empezamos con materias específicas. En cuanto a los horarios, suelen ser complicados para trabajar y estudiar a la vez. Las materias están muy buenas, son variadas y aprendemos de todos los compartimentos ambientales: aire, agua, suelos y biota. El fuerte de nuestro título es el diseño de sistemas de tratamiento de efluentes líquidos y para recibirnos tenemos que hacer un proyecto final. Te invitamos a ver las presentaciones de los proyectos para que sigas aprendiendo qué hacemos en ingeniería ambiental!

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬[Ing. Ambiental](https://chat.whatsapp.com/LLLeYk9jwkr5my3zMLFfN5)


## 🗺️ Mapa de correlativas

![Correlativas-ing-ambiental](./plan_de_estudio_ing_ambiental_1.jpg)


## 📂 Carpeta Frubox

![ing-ambiental#1](/indexmenu>/materias/ing-ambiental#1)
