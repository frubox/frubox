# PPS

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

Para la PPS hay un pdf que adentro tiene el reglamento y los formularios a completar. **De todas formas antes de elegir un tema de PPS hay que chequear con el/la jefe/jefa de cátedra (por ahora es <fedebailat@gmail.com>) y tener su autorización**. El tema tiene que estar relacionado con la carrera, pero hay dentro de todo flexibilidad con el tema (algunos ejemplos son pasantías en industrias, trabajo como inspector de industrias en la Municipalidad de San Martín, trabajo en un laboratorio en marco de una Beca PEFI o CIN, son algunos ejemplos de PPS que ya se hicieron). La cursada se aprueba y regulariza una vez que se dio la presentación, se entregó el informe, y estando previamente anotades en la materia, nos pasan la nota de final.

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox 

<iframe src="https://drive.google.com/embeddedfolderview?id=1IHzc5RQGqncJL-cXsHwPLGlEDlz7Lg9P" style="width:100%; height:600px; border:0;"></iframe>
