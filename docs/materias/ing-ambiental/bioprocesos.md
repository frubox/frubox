# Bioprocesos

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

Con respecto a la materia en sí, es una !@#\$% (no se si lo quieren poner en la descripción de la materia en frubox), el profesor es totalmente desorganizado, da clases de una hora y despues se va porque tiene "reuniones". La ayudante a cargo no esta capacitada. No hay mucha bibliografía... lamentablemente la materia esta MUY buena pero el docente a cargo no es DOCENTE.

Es ***fuertemente*** recomendada la lectura del libro de Pauline Doran, Bioprocess Engineering Principles (está en la sección de libros de LBT).

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox
 

<iframe src="https://drive.google.com/embeddedfolderview?id=1ypFmm4vxyrXzrDb97CSBIaDrIuJpN_RC" style="width:100%; height:600px; border:0;"></iframe>
