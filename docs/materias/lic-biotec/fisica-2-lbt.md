# Física II (LBT)

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

Holaa, estoy preparando el final y quería dejar mi recomendación para preparar la materia y cursarla de forma llevadera.

Física II es una materia que si la podés mantener al día con las teóricas (fijándote de entender a fondo los ejemplos de aplicaciones que se dan) y prácticas se puede llevar bien... Si la dejás estar es levantable pero para nada recomendable... si termina siendo el caso, el Tipler Volumen 1 y 2 tienen los contenidos de forma muy parecida y con ejemplos adicionales de aplicación que están buenos.

Si tenés el tiempo, creo que una buena idea es leer los capítulos antes de ir a las clases como para saber qué se va a explicar y poder cerrar los contenidos en el mismo día que se ven.

Para los parciales y recuperatorios es recomendable hacer parciales (o sea la complejidad de los ejercicios es más elevada que la de las guías y algunos implican tener clara la teoría). El final lo preparé con el Tipler y buscando entender bien qué supuestos son necesarios para poder tener la ecuación, por ejemplo : ¿por qué el campo eléctrico dentro de un conductor es 0? Si es oral, como en cualquier otra materia si podés prepararlo con alguien se hace más llevadero :).

Durante la cursada, cuando se acercan los Tps lo mejor es encontrar un buen grupito de compas e intentar hacerlo con el mayor tiempo posible para fijarse muuucho en la redacción y darse el tiempo para entender, son bastante exigentes con las correcciones.

Éxitos 😊

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

[Física IIA (LBT)](https://chat.whatsapp.com/FUwA8MME7vOHE20fzNf7z0)

## 📂 Carpeta Frubox

<iframe src="https://drive.google.com/embeddedfolderview?id=1bN0d---DL2UwNP66vydNPYQjhg1krslF" style="width:100%; height:600px; border:0;"></iframe>
