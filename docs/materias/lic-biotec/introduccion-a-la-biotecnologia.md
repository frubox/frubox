# Introducción a la Biotecnología

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 [Intro a la Biotec](https://chat.whatsapp.com/CDWFzeKDmeY2CmOXqDt7OX)

## 📂 Carpeta Frubox

???+ note "Esta materia no tiene descripción"
    
    En el primer cuatrimestre de 2019 la materia se encuentra en transición entre la del plan viejo (de 4to año) y la del plan nuevo (de 1ro). Los contenidos en la carpeta 'transición' seguramente sean mas de lo que terminen dando, pero seguro algo rescatan.


**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**


<iframe src="https://drive.google.com/embeddedfolderview?id=1IIiXf-Uwp-JFPc3XRS3rUKvGEpKKybSX" style="width:100%; height:600px; border:0;"></iframe>
