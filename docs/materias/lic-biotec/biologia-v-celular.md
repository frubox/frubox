# Biología Celular (V)

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)



## 💭 Experiencias

Esta materia tiene un doble filo.

Por un lado las teóricas son completísmas y pienso que el objetivo es lograr una visión global y a la vez detallada de cómo funciona una célula eucariota (tirando a mamífera). Los prácticos son buenos.

Por otro lado el modo de evaluación de las teóricas es **multiple choice enormes** con muy poco para pensar. Digamos que nos dejó deseando una evaluación más conceptual.


✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**


<iframe src="https://drive.google.com/embeddedfolderview?id=1pupQCrmbNupaV08nqf6R2LB4sZ5WsuE7" style="width:100%; height:600px; border:0;"></iframe>
