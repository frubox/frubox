# Microbiología General

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

Debe ser la materia con los prácticos más escenciales de toda la carrera. Nos dimos cuenta de que antes de esta materia no sabíamos **hacer** las cosas más sencillas del trabajo científico de rutina más básico que se hace en los laboratorios del IIB.

Las teóricas fueron *buenas* y las evaluaciones razonables. Sin embargo creemos que hay para mejorar en varios aspectos, y es una de las primeras materias muy dificil de evaluar globalmente por la cantidad y variedad de docentes que la imparten.

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**

<iframe src="https://drive.google.com/embeddedfolderview?id=1QbcFdzjqYq4Rl5Pz1STzE-FUqDM4tca9" style="width:100%; height:600px; border:0;"></iframe>
