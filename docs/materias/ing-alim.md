# Alimentos

???+ question
    He aquí la sección para la carrera de Ingeniería en Alimentos, felicidades por abrirla :)

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    🍅 🍋 🍉 🍊 🍇 🥑 🌶 🍒 🍍 🥝 🍓 🍎 🫐 🥥


📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

<iframe src="https://drive.google.com/embeddedfolderview?id=17m0OHsth-4vSWXkiwHLOhP5YF20X3GIr" style="width:100%; height:600px; border:0;"></iframe>
