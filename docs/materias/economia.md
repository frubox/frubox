# Economia y Negocios

???+ question Esta sección no tiene biografía
    Escribinos a <unsam@frubox.org> con tus aportes para expandir esta sección.

    :tomato: :lemon: :watermelon: :tangerine: :grapes: :avocado: :hot_pepper: :cherries: :pineapple: :kiwi: :strawberry: :apple: :blueberries: :coconut:


📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**

<iframe src="https://drive.google.com/embeddedfolderview?id=1hkT-rQ0Q-2OzoSxlofNPvAP4DvJS_Um6" style="width:100%; height:600px; border:0;"></iframe>
