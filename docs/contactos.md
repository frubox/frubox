# Emails Docentes y Oficinas ☎️

???+ question
    Seguramente muchos emails hayan cambiado.
    
    Si ves algun contacto que está viejo, avisanos y/o mandanos nueva info a <unsam@frubox.org>.

    :tomato: :lemon: :watermelon: :tangerine: :grapes: :avocado: :hot_pepper: :cherries: :pineapple: :kiwi: :strawberry: :apple: :blueberries: :coconut:

Información de contacto para oficinas, fotocopiadoras, docentes, autoridades, secretarías y profes particulares de la Unsam!

> Lo que falte o no ande, por favor pasalo en un email a <unsam@frubox.org>

> Ctrl+F para buscar

> Si tu información aparece acá y preferís que no esté, avisanos por mail y compartinos una forma de contacto alternativa.

No está ordenado ni es consistente con el nombre de las materias, así que te sugerimos insistir con el Ctrl + F para encontrar lo que buscás.

Si en la lista figura algun profe con mas de un 1 (un) email, es recomendable que le envies email a todos los correos que figuren de ese mismo profe para asegurarte que les llego tu correo.

## Formulario para agregar info

Formulario de google para mandar contactos nuevos: <https://forms.gle/2AzKQuhVLquczzqe9>

Nueva info de contacto (puede no estar abajo): <https://docs.google.com/spreadsheets/d/1iscTBvyfsSIRfrITWILVph-sNUpr-OnXFLAI8uAgW3E/edit?usp=sharing>

## Contacto Oficinas

Biblioteca Central CyT y Humanidades <bc@unsam.edu.ar>

Fotocopiadora CyT <centrofotocenri@hotmail.com>

Fotocopiadora Humanidades <fotocopiadorascassini@yahoo.com.ar>

Programa de Pedagogía Universitaria UNSAM <peunsam@unsam.edu.ar>

ECyT <escuelacyt@unsam.edu.ar>

Oficina de Alumnos CyT <cytdga@unsam.edu.ar>

Centro de Atención Psicoanalítico (CAP) <cepunsam@gmail.com>

## Contacto Docentes

> Si ves información de contacto que sea "personal", por favor avisanos para que podamos revisar y ajustar este sitio con información de contacto oficial.

Federico Venezia <feedevenezia@gmail.com> Matematica Fisica

Carolina Leguizamón <caroleguizamon5@hotmail.com>

Corina Averbuj <caverbuj@unsam.edu.ar > <caverbuj@dm.uba.ar> <caverbuj@uade.edu.ar> (Cálculo 1)

Rosa Piotrkowski <rosap46@yahoo.com> ¿ <rosap@unsam.edu.ar> ? (Calculo 2)

Marcela Morvidone <morvidone@gmail.com> (Calculo 3)

Marcela Fabio <satanas666@gmail.com>

Nicolas Vorobioff <nborovio@hotmail.com> (Intro. al analisis matemático Matemática CPU)

Gabriel Duarte <gduarte@udesa.edu.ar>

Edgardo Savoini <edsavoini@gmail.com> (Seguridad e Higiene)

Mariel Rossenblat <mrosen@ungs.edu.ar>

Salvador Gil <sgil@unsam.edu.ar> <sgil@df.uba.ar> <sgil1950@gmail.com> (Física III Secretario academico de CyT)

Francisco Parisi <fparisi@unsam.edu.ar> <parisi@cnea.gov.ar> (Físicas I y IV Decano escuela ciencia y tecnología)

Gabriela Lagorio <mgl@qi.fcen.uba.ar>

Hugo Negrin <ghnegrin@gmail.com> <hugonegrin@fibertel.com.ar> <hnegrin@fibertel.com.ar> (Jefe de Álgebra 1 y Director del CPU)

Julián <Julianfm7@gmail.com> (Matematica 1)

Mariel Rossenblat <mrosen@ungs.edu.ar> (Introduccion al analisis matematico)

Carlos Buscaglia <cbusca@iib.unsam.edu.ar> <cbuscaglia89@gmail.com> (Genética general)

Andrea Martins <amartins@inti.gob.ar> (Introducción a la Calidad)

Silvia Diaz Monnier <silviadm@inti.gob.ar> (Introducción a la Calidad)

¿? <s.diaz@unsamdigital.edu.ar> (Introducción a la Calidad)

Beatriz Bargiela <bargielab@yahoo.com.ar> (Sistemas de Representación Gráfica)

Raúl Varela <raul_varela@cosellama.com.ar> (Sistemas de Representación Gráfica)

Alejandra Goldman <agoldman@unsam.edu.ar> <agoldman64@gmail.com> (Biología 1 Inmuno. Básica)

Mónica A. Palmieri <monica.ale.palmieri@gmail.com> (Biología 2)

Carlos Amorena <camorena@unsam.edu.ar> (Biología 3)

Rosalía <losviajeros@hotmail.com> (Biología 3)

Alfredo Olivero <aolivero@unsam.edu.ar> (Bases de datos, TPI)

Javier Guevara <javier.guevara@unsam.edu.ar> <jguevara@unsam.edu.ar> <guevara@cnea.gov.ar> <javierguevara2004@yahoo.com.ar> (Física II Secretario Académico)

Javier Rodriguez <jrodrigu@tandar.cnea.gov.ar> <javier@speedy.cnea.gov.ar> (Quimica CPU Jefe departamento Química)

Nora Iñón <norai@iib.unsam.edu.ar> (Microbiología General)

Diego Comerci <dcomerci@iibintech.com.ar> (Microbiología General)

Sánchez <dsanchez21@gmail.com> (Química Biológica Genética Humana)

Susana Giambiagi <sgiambi@iibintech.com.ar> (Genética Molecular)

Viviana Lepek <vlepek@iibintech.com.ar> (Microbiología General Métodos de Análisis Biomédicos)

Juan Ugalde <jugalde@iibintech.com.ar> (Genética General)

Andrés Cioccini <aciocchini@iibintech.com.ar> (Química Biológica Microbiología)

JJ. Cazzulo <jcazzulo@iibintech.com.ar> (dios del universo)

Vanina Álvarez <vanina.eder.alvarez@gmail.com>

Fabiana Fulgenzi <fabyfulgenzi@gmail.com> (Biología IV)

Guillermo Santamaría <gsantama@intech.gov.ar> (Biología IV)

Esteban <ehernand@agro.uba.ar> (Biología IV)

Paula <pauladel@gmail.com> (Biología IV)

Maria Claudia Abeledo <cabeledo@linti.unlp.edu.ar> <mcabeledo@gmail.com> (Redes locales, TPI/TRI)

Eduardo Serrano <eduardo.eduser@gmail.com> (Métodos Numéricos)

Claudia Ferragut <ingferragut@hotmail.com> (Estática y resistencia de los materiales)

Ariel Mayo <ariel70.mayo@gmail.com> (Introducción a los estudios universitarios, CPU)

Fernanda Lopez <fernanda91.lopez@gmail.com> (Ayudante de Ariel Mayo, IEU CPU)

Myriam Ford <mford@unsam.edu.ar> (Introducción a los estudios universitarios, CPU)

Intro. informatica T.M <informatica.unsam@gmail.com> (Introd.informática T. Mañana)

M. Laura Japas <mljapas@gmail.com> (Quimica)

Beatriz Artesi <bartesi@fnls.com.ar> / <Bartesi@gmail.com> (Intro. analisis matemático Matemática CPU)

Daniel Di Gregorio <digregorio@unsam.edu.ar> (Física CPU Vicedecano universidad)

Jorge Sinderman <sinderman@unsam.edu.ar> (Teoría de circuitos Director electronica)

Gabriela Lagorio <mgl@qi.fcen.uba.ar> (Quimica)

Eduardo Serrano <eduardo.eduser@gmail.com> (Metodos numéricos)

Adrian Alvarez <aalvarez@ungs.edu.ar> / <adalvarez@unsam.edu.ar> (Métodos Numéricos, Probabilidad y Estadística, Matemática I Tec T.M)

Diego Melchiori <dmelchiori@yahoo.com.ar> (Matemática CPU / Matemática 1 Tec -T.M- / Prob. y estadistica)

Roberto Ben <benroberto@gmail.com> (Matematica 1 Tec T.M, Calculo 1)

Juan Esperón <Prof.Esperon@gmail.com> (Matematica 1 Tec T.N)

Ana ruedin <ana_ruedin@yahoo.com.ar> (Algebra II)

Maria E. Reinoso <mariae.reinoso@gmail.com> <reinoso@tandar.cnea.gov.ar> (Fisica CPU Electricidad y Magnetismo TPI/TRI/TEM/TUAN)

Pablo Shurman <pabloshurman@unsam.edu.ar> (Intro.Informática Laboratorio de computación 1)

Ariel Arbiser <arbiser@dc.uba.ar> (Metodos Numericos TPI/TRI)

Ernesto Roclaw <eroclaw@unsam.edu.ar> (Laboratorio de computación 1)

Ignacio J. General <ijgeneral@gmail.com> (Fisica IV ; LBT)

Susana Granado Peralta <sgpsusana@infovia.com.ar> <sasgp70@hotmail.com> (Matematica 3 TPI/TRI)

Carlos Scirica <cscirica@dm.uba.ar> (Matematica 2 TPI/TRI)

Marcela Fabio <cela.fabio@gmail.com> <mfabio@unsam.edu.ar> (Calculo 3)

Gonzalo Comas <gcomas@dm.uba.ar> <g.comas@unsamdigital.edu.ar> (Introducción al análisis matemático)

Diego Noseda <diegonoseda@yahoo.com.ar> (Bt. Medicamentos)

Monica Gabay <monagabay@yahoo.com> (Etica y Deontologia)

Monica Cuschnir <monicac@unsam.edu.ar> (Introd informática, Turno mañana)

Guido Ianni <guido_ianni@yahoo.com> (Micro/Macroeconomia)

Maria Gabriela Mataloni <gmataloni@unsam.edu.ar> (Biología 2, ingeniería ambiental)

Fabio Brushetti: <fbrusche@gmail.com> (Profesor Teoria - Sistema de proc. de datos)

Gaston Aguilera: <rgarcia@cnea.gov.ar> (Profesor Practica - Sistema de proc. de datos y sistemas operativos)

Juan Lopez y/o Monica Hencek <informatica.consultas@gmail.com> (Lab computación 2)

Juan Lopez <jlopez@unsam.edu.ar> (Profesor Informatica)

Maria Eugenia Capoulat <mcapoulat@gmail.com> (Fisica 1)

Andres Kreiner <kreiner@tandar.cnea.gov.ar> (Fisica 4)

Debray <debray@tandar.cnea.gov.ar> (Laboratorio Fisica 4)

Ariel <aridec@gmail.com> (Fisico-Quimica)

Maria Ines Troparevsky <mariainestro@gmail.com> <mitropa@fi.uba.ar> (Calculo 1)

Diana Rubio <diarubio@gmail.com> <drubio@unsam.edu.ar> (Calculo 2)

Juan Pedrosa <pedrosa@tandar.cnea.gov.ar> (Fisica CPU)

Alberto Lamagna <alamagna@cnea.gov.ar> (Electricidad y Magnetismo TPI/TRI)

Hugo Huck <huck@cnea.gov.ar> (Electricidad y Magnetismo TPI/TRI/TEM/TUAN Fisica CPU)

Alejandro Bompensieri <abompensieri@unsam.edu.ar> <a.bompensieri@unsamdigital.edu.ar> (Introducción a la informática T.N)

Monica Martínez Bogado <mbogado@tandar.cnea.gov.ar> (Fisica CPU)

Javier Garcia <jgarcia@tandar.cnea.gov.ar> (Fisica CPU, ayudante de Monica)

Maria Lujan Ibarra <mibarra@unsam.edu.ar> (Quimica CPU)

Andrea Gioffré <gioffre.andrea@inta.gob.ar> <agioffre@cnia.inta.gov.ar> (Biología 2)

Roberto Candal <candal@qi.fcen.uba.ar> (Quimica CPU)

Adrian Mutto <aamutto@gmail.com> (LBT)

Daniel Vega <vega@cnea.gov.ar> <dvega@uns.edu.ar> (Fisica 1 TEM)

Andres Alonso <alonso@cnea.gov.ar> (Informatica)

Analia Iriel <iriel.analia@gmail.com> (Quimica)

Gustavo Curutchet <gustavo.curuchet@unsam.edu.ar> (Coordinador Lic. Ambiental)

Daiana Martinez <dmartinez@unsam.edu.ar> (Ayudante Introd.informatica T.M)

Martin Blasco <elmaker@gmail.com>

Diana Mielnicki <dmielnicki@unsam.edu.ar> <dianamiel@gmail.com> (CTS)

Maria Eugenia Capoulat <mcapoulat@gmail.com> (Fisica 1)

Estefania <estefaniadal@yahoo.com.ar> (Sistemas de Representacion Grafica)

Martin Torreblanca <martint@inti.gob.ar> (Mecanismos)

Estela <smmb04@gmail.com> (Coordinadora CTS)

Susana Giambiagi <sgiambi@iibintech.com.ar> (LBT)

Paola Babay <babay@cnea.gov.ar> (Quimica General)

Guillermo La Mura <glamura@unsam.edu.ar> (Director Tecnicatura Electromedicina)

Alberto Kohen <ajkohen@yahoo.com> (Electromedicina)

Daniel Saulino <saulinodaniel@yahoo.com.ar> (Electromedicina)

Lelia Dicelio <lelia.dicelio@gmail.com> (Quimica Inorganica Quimica CPU)

Daiana Figueroa <daiu.figueroa@gmail.com> (Fisica CPU)

Rinaldi <rinaldi@cnea.gov.ar> (Fisico-Quimica)

Diego Rubi <diego.rubi@gmail.com> (Fisica 3)

Maugeri <maugeri@iibintech.com.ar> (Quimica Biologica)

Lucas Bali <lbali@dm.uba.ar> <lucasbl3@yahoo.com> (Probabilidad y estadistica)

Silvia Farina <farina@cnea.gov.ar> (Fisica 2)

Eduardo Pallone <epallone@unsam.edu.ar> (Soporte Tecnico - Laboratorio 1 )

Rafael Grimson <rgrimson@unsam.edu.ar> (Analisis Matematico)

Fisica CPU (email oficial de la catedra) <cpufisicaunsam@gmail.com>

Cristian Ferreyra <cferreyra@gmail.com> (Ayudante Fisica 3)

Introducción a la informatica (T.N) <consultasintro@gmail.com> (email de la cátedra turno noche)

Maria Dolores Perez <mdperez@tandar.cnea.gov.ar> (Quimica CPU)

Lidia Montes <lmontes@inti.gob.ar> (INCALIN - Ing Alimentos)

Gustavo Duffo <duffo@cnea.gov.ar> (Quimica Organica)

Mario Debray <debray@tandar.cnea.gov.ar> (ayudante tps Fisica 4)

Nahuel Agustin Vega <nahuelv@gmail.com> (ayudante Fisica 4)

Carlos Arregui <carregui@iib.unsam.edu.ar> (Biologia Celular)

Andres Zelcer <zelcer@cnea.gov.ar> <andres.zelcer@cibion.conicet.gov.ar> (Quimica General)

Esteban Clavero <estebanclavero@gmail.com> <esteban.clavero@unq.edu.ar> (Quimica)

Eduardo Vergini <vergini@tandar.cnea.gov.ar> (Fisica 1)

Brenda Lugo <brenlu87@gmail.com> (Física CPU)

Belen Parodi <belen@inti.gob.ar> (Quimica General)

Ornella Mahiques <ornemahiques@hotmail.com> (Ayudante Fisica CPU)

Leon Braunstein <leonbraunstein@gmail.com> (Calculo)

Mariel Rosenblatt <mrosen@dm.uba.ar> <mrosen@ungs.edu.ar> (Introd analisis matematico)

Quimica CPU (email oficial de la catedra) <cpuquimicaunsam@gmail.com>

Juan Pablo Borgna <jpborgna@ungs.edu.ar> <jpborg@gmail.com> (Matematica 2 TPI)

Mara Roset <mroset@iibintech.com.at> (Metanal y Microbiologia)

Mariano Quinteros <mquinter@cnea.gov.ar> (Fisica 1)

Jose L. Marco-Brown <josemarcobrown@gmail.com> <joseluis.marcobrown@unsam.edu.ar> (Quimica General)

Lucas Insua <lucasinsuaa@gmail.com>

Giuliana Saggion giulisaggion@hotmail.com

Javier Cebeiro <jcebeiro@garrahan.gov.ar> javiercebeiro@yahoo.com.ar

Silvia <silvia.cecere@gmail.com> (Algebra 1)

Mariela Del Giudice ¿? (Métodos de Análisis Biomédicos)

Lucas Spigariol <lspigariol@gmail.com> (Algoritmos I)

Ezequiel Dratman <zequieldratman@gmail.com> (Metodos numericos)

Julieta Molina <molina_julieta@yahoo.com.ar> (Estadística Aplicada/Bioestadística)
