# Libros y Bibliotecas 📚

Bibliografía para estudiar las materias de la UNSAM :) organizada por área del conocimiento aproximada.

-   [Química](https://drive.google.com/drive/folders/0B5fZxPnEUqIMfk1TWjhjd29rSTZ3WmRBdWQtVDdqR1lUbmdka2lzcEgtZHNjRE0wZXN3SHM?resourcekey=0-uG_FiQqgaDpTSjOWKV4-Rw&usp=sharing)
-   [Biotecnología](https://drive.google.com/drive/folders/0B5fZxPnEUqIMfkZXcmtlRmN0NDZPb0R0cmpMNHBiREdJcFZCSmUxZFZlR0lDRmFTWGNUdVk?resourcekey=0-PW5z8Kc1gpp9VQwxEGKxGg&usp=sharing)
-   [Biología](https://drive.google.com/drive/folders/0B5fZxPnEUqIMflUxUnpEQ0FFTWFCRTNiZ3VxZ1QyTWhOSGxXTDBfT3NBUTh5MWhlTDdMN2c?resourcekey=0-IsejSNMQobVnhBKrwE40YQ&usp=sharing)
-   [Física](https://drive.google.com/drive/folders/0B5fZxPnEUqIMfmRBbk9BMFRfaG1jTHo2VlhTUFI0QUx6Qk1qUFlWek5EWTFyeVZhNk5SeTg?resourcekey=0-TkzgSMgCQSpb27zQJQwbsw&usp=sharing)
-   [Matemática y Estadística](https://drive.google.com/drive/folders/0B5fZxPnEUqIMfmhrZHFERk1hVTBOeWVRSTNRSDBnR2V3a3YzTFU1OWxCTk8xZGd6dXhaVzQ?resourcekey=0-jwgBoCmliUSXJUnjvGsX8w&usp=sharing)
-   ¿Tu libro no está? Escribinos a <unsam@frubox.org> y te ayudamos a conseguirlo, o si lo tenés, enviánoslo y lo subimos :)

> **Si no ven los libros** más abajo, o parece que no tienen permiso, quizás necesiten tener iniciada la sesión en Google/Gmail.etc, o usar los links anteriores.
>
> Si igual no pueden ver los libros, por favor avisanos por <unsam@frubox.org> o [instagram](https://www.instagram.com/frubox/).

![20211107-202944.png](./img/20211107-202944.png)

## Libros por área

### Biologías

  <iframe src="https://drive.google.com/embeddedfolderview?id=0B5fZxPnEUqIMflUxUnpEQ0FFTWFCRTNiZ3VxZ1QyTWhOSGxXTDBfT3NBUTh5MWhlTDdMN2c&resourcekey=0-IsejSNMQobVnhBKrwE40YQ&usp=sharing" style="width:100%; height:600px; border:0;"></iframe>

### Química

<iframe src="https://drive.google.com/embeddedfolderview?id=0B5fZxPnEUqIMfk1TWjhjd29rSTZ3WmRBdWQtVDdqR1lUbmdka2lzcEgtZHNjRE0wZXN3SHM&resourcekey=0-uG_FiQqgaDpTSjOWKV4-Rw&usp=sharing" style="width:100%; height:600px; border:0;"></iframe>

### Matemática y Estadística

  <iframe src="https://drive.google.com/embeddedfolderview?id=0B5fZxPnEUqIMfmhrZHFERk1hVTBOeWVRSTNRSDBnR2V3a3YzTFU1OWxCTk8xZGd6dXhaVzQ&resourcekey=0-jwgBoCmliUSXJUnjvGsX8w&usp=sharing" style="width:100%; height:600px; border:0;"></iframe>

### Física

  <iframe src="https://drive.google.com/embeddedfolderview?id=0B5fZxPnEUqIMfmRBbk9BMFRfaG1jTHo2VlhTUFI0QUx6Qk1qUFlWek5EWTFyeVZhNk5SeTg&resourcekey=0-TkzgSMgCQSpb27zQJQwbsw&usp=sharing" style="width:100%; height:600px; border:0;"></iframe>

### Biotecnología

  <iframe src="https://drive.google.com/embeddedfolderview?id=0B5fZxPnEUqIMfkZXcmtlRmN0NDZPb0R0cmpMNHBiREdJcFZCSmUxZFZlR0lDRmFTWGNUdVk&resourcekey=0-PW5z8Kc1gpp9VQwxEGKxGg&usp=sharing" style="width:100%; height:600px; border:0;"></iframe>

### Ingenierías

???+ question
    No tenemos libros de estas carreras por ahora, [mandanos](unsam@frubox.org) sugerencias por email. :)

## Otros sitios con libros

[Biblioteca académica](https://www.google.com/url?q=https%3A%2F%2Fmega.co.nz%2F%23F!59FAiQTT!T6cdc3K-8XR85ANxBnFYag&sa=D&sntz=1&usg=AOvVaw3IVGa3vawAcYYKridcPFC0)

[Biblioteca filósofo-política](https://www.google.com/url?q=https%3A%2F%2Fwww.dropbox.com%2Fsh%2Fbh0d2w8xqfasjg4%2FAACHLdjJi2xx95_5YD8GuCIPa%3Fdl%3D0&sa=D&sntz=1&usg=AOvVaw0NmePcwzj9Ywpb9BrHs2WP)

[Química Analítica y Biología Celular](https://www.google.com/url?q=https%3A%2F%2Fmega.nz%2F%23F!nddAibII!b3zpcXYAn7kerjPxQNBOwg&sa=D&sntz=1&usg=AOvVaw0oNfrCCQkY0PAbr301tF1G) (Mega)

[Química Analítica y Biología Celular](https://drive.google.com/folderview?id=0B2La4vudPouWfnQ1UTFSLThPbEZlYWJ3RFNBbTR6aHB6a0NSYjhnUThzc0dUN0tOTUJRSTA&usp=sharing) (Drive)

## Libros Varios

Biblioteca Popular de PDFs usados, deposite aquí! Carpeta de edición libre.

<iframe src="https://drive.google.com/embeddedfolderview?id=0B5fZxPnEUqIMfk5wc1c3YlFpc2UwajM4QjkxMHNmNnU3cUVNM1U5QkJuckRNX0pURWNsaG8&resourcekey=0-2LTUxtLFrPvJt5y18pTgoA&usp=sharing" style="width:100%; height:600px; border:0;"></iframe>
