# 🌏 Sitio web

Documentación de uso y administración de este sitio web :)

## 💭 Como aportar a Frubox

Mandanos un mail con tu aporte a <unsam@frubox.org>, y nosotres nos encargamos de acomodarlo en la sección correcta.

## ✏️ Cómo editar el sitio

La forma más fácil para vos es que lo hagamos nosotros. Mandanos un mail con lo que querés agregar a <unsam@frubox.org> y lo hacemos.

También podés usar el botón de "editar" que aparece arriba a la derecha. Eso te va a llevar a nuestro repositorio de GitLab. Vas a necesitar:

- Una cuenta en [gitlab.com](https://gitlab.com)
- Conocimiento básico de cómo escribir texto en "markdown".
